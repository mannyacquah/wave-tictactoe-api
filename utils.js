// Play constants representing char plays
const PLAY_X = 'x'
const PLAY_O = 'o'
const BOARD_SIZE = 9;

/**
 * Error Handler - Really primitive due to time constraints to log to console
 * @param {string} error 
 */
const handleError = (error) => {
    console.log(error)
}

/**
 * Gets the number of plays present in a given board using the play char
 * @param {string} board 
 * @param {char} play 
 * 
 * @return {string}
 */
const getNumberOfPlays = (board, play) => {
    return (board.match(new RegExp(play, 'g')) || []).length
}

/**
 * @param {string} board 
 * 
 * @return {bool}
 */
const validateBoard = (board) => {
    const validationRegex = /^[xo\s]*$/

    if (board.length !== BOARD_SIZE) {
        handleError(`Not ${BOARD_SIZE} digits`)
        return
    }

    if (!validationRegex.test(board)) {
        handleError('Invalid values in board')
        return
    }

    let xPlays = getNumberOfPlays(board, PLAY_X)
    let oPlays = getNumberOfPlays(board, PLAY_O)
    if (Math.abs(xPlays - oPlays) > 1 || oPlays - xPlays > 0) {
        handleError(`Who's cheating?!! ;)`)
        return
    }

    return true
}

/**
 * Inserts the required play and returns new updated board
 * 
 * @param {string} board 
 * 
 * @return {string}
 */
const buildNewBoard = (board) => {
    return board.replace(' ', PLAY_O)
}

module.exports = {
    BOARD_SIZE,
    buildNewBoard,
    validateBoard,
}