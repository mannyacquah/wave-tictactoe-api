# wave-tictactoe-api


## Running Locally

```sh
$ git clone git@bitbucket.org:mannyacquah/wave-tictactoe-api.git
$ wave-tictactoe-api
$ make init
```

Your app should now be running on [localhost:3000](http://localhost:3000/).

## Deploying

```
$ make deploy
```