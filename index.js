const express = require('express')
const app = express()
const router = express.Router()
const utils = require('./utils')
const PORT = process.env.PORT || 3000

// Middleware to validate board
router.use(function (req, res, next) {
    let board = req.query.board

    // Send to error router if board request param is empty
    if (undefined === board) return next('router')

    // Ensure empty sections in board string are filled out
    let paddedBoard = board.padEnd(utils.BOARD_SIZE)

    // Validate board and send to error router if validatation fails
    if (!utils.validateBoard(paddedBoard)) return next('router')

    // Set board in req to padded board for further processing
    req.query.board = paddedBoard

    next()
})

// Return new board as string
router.get('/', function (req, res) {
    res.send(utils.buildNewBoard(req.query.board))
})

// Send 400 if router fails
app.use('/', router, function (req, res) {
    res.sendStatus(400)
})

app.listen(PORT, () => console.log(`Listening on ${ PORT }`))