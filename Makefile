.PHONY: all deploy logs help

all: help

deploy: ## push to heroku
	git push heroku master

init: ## install dependendies and run app
	npm install && npm start

logs: ## display app logs
	heroku logs --tail

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
